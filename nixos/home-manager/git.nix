{ config, pkgs, lib, ... }:
let
  userName = "Relief Melone";
  userMail = "relief.melone@gmail.com";
in
{
  home-manager.users.${config.mainUser} = {
    programs.git = {
      enable = true;
      userEmail = "${userMail}";
      userName = "${userName}";

      extraConfig = {
        init = {
          defaultBranch = "main";
        };
      };
    };
  };
}
