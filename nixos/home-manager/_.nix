{ config, pkgs, lib, ... }:
let
  release = "23.05";
  home-manager = builtins.fetchTarball "https://github.com/nix-community/home-manager/archive/master.tar.gz";
in
{
  imports = [
    (import "${home-manager}/nixos")
    ./git.nix
  ];
  
  home-manager.useGlobalPkgs = true;

  home-manager.users.${config.mainUser} = {
    home.stateVersion = "${release}";
  };

}
