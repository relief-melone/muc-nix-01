{ config, pkgs, ... }:
{
  users.users.${config.mainUser} = {
    packages = with pkgs; [
      kubectl
      kubernetes-helm
#      nodejs_20
#      argocd
#      packer
    ];
  };
}
