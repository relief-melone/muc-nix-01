{ config, pkgs, lib, ... }:
with lib;
with types;
{

  imports = [
    ./user-packages.nix
  ];

  options = {
    mainUser = mkOption {
      type = str;
      description = "main user for the operating system";
    };
  };

  config = {
    mainUser = "relief-melone";
  };
}
