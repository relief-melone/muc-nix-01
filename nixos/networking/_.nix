{ config, pkgs, ...}:
{
  networking = {
    hostName = "muc-main-01";
    networkmanager = {
      enable = true;
      insertNameservers = [
        "8.8.8.8"
      ];
    };
    extraHosts = ''
      192.168.178.10 	rancher.relief-melone.muc
    '';

    hostFiles = [
      /etc/nixos/networking/_hosts
    ];

    firewall = {
      allowedTCPPorts = [ 53 ];
      allowedUDPPorts = [ 53 ];
    };
  };

  users.users.${config.mainUser} = {
    extraGroups = [ "networkmanager" ];
  };

  services.unbound = {
    enable = true;
    settings = {
      server = {
        interface = [ "0.0.0.0" "192.168.178.10" ];
        local-data = [ 
	  "\"rancher.relief-melone.muc A 192.168.178.10\"" 
	  "\"argo.relief-melone.muc A 192.168.178.10\""
	];
        access-control = [
          "0.0.0.0/0 allow"
          "192.168.0.0/16 allow"
        ];
      };

      remote-control.control-enable = true;
    
    };
  };

  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

}
