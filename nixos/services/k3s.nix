{ config, pkgs, ...}:
let
  token = (import ../credentials/_.nix).k3s_token;
in
{

  networking.firewall.allowedTCPPorts = [ 6443 ];
  boot.kernelModules = [ "ceph" ];

  services.k3s = {
    enable = true;
    role = "server";
    token = "${token}";
    clusterInit = true;
    extraFlags = toString [
      "--container-runtime-endpoint unix:///run/containerd/containerd.sock"
      # "--disable traefik"
      # "--disable local-storage"
      # "--node-taint CriticalAddonsOnly=true:NoExecute"
    ];
  };

  virtualisation.containerd = {
    enable = true;
    settings =
      let
        fullCNIPlugins = pkgs.buildEnv {
          name = "full-cni";
          paths = with pkgs;[
            cni-plugins
            cni-plugin-flannel
          ];
        };
      in {
        plugins."io.containerd.grpc.v1.cri".cni = {
          bin_dir = "${fullCNIPlugins}/bin";
          conf_dir = "/var/lib/rancher/k3s/agent/etc/cni/net.d/";
        };
      };
  };

  environment.systemPackages = with pkgs; [
    k3s
  ];

  systemd.services.k3s = {
    wants = [ "containerd.service" ];
    after = [ "containerd.service" ];
  };

  systemd.services.containerd.serviceConfig = {
      ExecStartPre = [
        "-${pkgs.zfs}/bin/zfs create -o mountpoint=/var/lib/containerd/io.containerd.snapshotter.v1.zfs zroot/containerd"
      ];
    };

  services.openiscsi = {
    enable = true;
    name = "<some-name>";
  };
}
