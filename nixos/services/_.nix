{ config, pkgs, ...}:
{
  imports = [
    ./k3s.nix
    ./openssh.nix
  ];
}
