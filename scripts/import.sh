#!/usr/bin/env bash

BASEDIR=$(dirname $0)

echo "emptying current nixos folder"
rm -rf $BASEDIR/../nixos/*
cp -R /etc/nixos/* $BASEDIR/../nixos/

if [ ! -d $BASEDIR/../credentials ]; then
 echo "creating $BASEDIR/../credentials"
 mkdir $BASEDIR/../credentials
fi

echo "importing from /etc/nixos"
mv  $BASEDIR/../nixos/credentials/* $BASEDIR/../credentials
rm -rf $BASEDIR/../nixos/credentials
